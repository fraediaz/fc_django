#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sys
import boto3
import time
import os

tiempo_inicio   = time.time()
ruta_archivos   = sys.argv
s3              = boto3.resource('s3')

def subir_pdf_cola(ruta):
        archivos = [f for f in os.listdir(ruta)]
        for x in archivos:
            segundos   = time.time()
            print(chr(27) + "[2J")
            print('Cargando archivos desde \n'+ruta)
            print('\n',x)
            s3.Object('afcdjango', x).upload_file(ruta+x)
        print('completado en '+str(round((time.time()-tiempo_inicio),3))+'segundos')

try:
    subir_pdf_cola((str(ruta_archivos[1])))
except IndexError as error:
    print(chr(27) + "[2J")
    print('Debes especificar una ruta\n')
    print('python ',ruta_archivos[0], 'c:/carpeta/para/subir/ \n\n\n')
except FileNotFoundError as error:
    print(chr(27) + "[2J")
    print('Asegurate de que sea una rúta válida\n\n\n')



