# Fast Check

Analisis de documentos con analitica de inteliegencia artidical



## Instalación del ambiente de desarrollo.

Los siguientes pasos son necesarios para habilitar un entorno de trabajo.
- [Git](https://git-scm.com/) 
- registrar cuenta en [GitLab.com](http://www.gitlab.com) 
- Instalación de [Python 3](https://www.python.org/downloads/)
- Instalación del administrador de paquetes python [PIP](https://pip.pypa.io/en/stable/installing/)
- instalación de [Visual Studio Code](https://code.visualstudio.com/)
- Intalar [AWS CLI SDK](https://docs.aws.amazon.com/es_es/cli/latest/userguide/install-cliv1.html)

### Respositorio Fast Check
url del repositorio: https://gitlab.com/fraediaz/fc_django.git

Comando para clonar el repositorio:
```bash
git clone https://gitlab.com/fraediaz/fc_django.git
```

administrador del repositorio [rcarreno@st.cl](mailto:rcarreno@st.cl)

### Compilación de fuentes

Prepara el entorno virtual de Python.
mas infotmación sobre los entornos virtuales: http://docs.python.org.ar/tutorial/3/venv.html

Creación del entorno virtual:
el siguiente comando debe ser ejecutado en la raiz del proyecto:
```Bash
    virtualenv .env --python=python3
```
Una vez ejecutado el comando se creará una carpeta oculta con el entorno de ejecución virtual de python.

Activar el entorno virtual de python
```Bash
    source .env/bin/activate
```

Desactivar el entorno virtual
```Bash
    deactivate
```

Instalar las dependencias del proyecto.
una vez que estemos con el entorno virtual activado se instalará la lista de paquetes que hasta el momento se están usando en el proyecto.
Estas dependenias se encuentran en el archivo `requirements.txt`


```Bash
    pip freee
```
para comprobar los paquetes instalados en nuestro entorno virtual lo podemos hacer mediante el comando:
```Bash
    pip install -r requirements.txt
```
Ahora se puede repetir el comando `pip freeze` y comprobar la lista de dependencias del proyecto tal y como se muestra en el siguiente ejemplo:
```Bash

```
## Install AWS

```Bash
    pip3 install awscli --upgrade --user
```

## Configuración AWS

```Bash
    pip3 install awscli --upgrade --user
    aws2 configure
```

Keys:
```Bash
    Access Key = AKIAZKRTLWBAZLIROJ75
    Secret Key = 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
    Region     = us-east-1
    Format     = json
```

## Configuración Keycloak
### Panel de administración Django
``` Panel de administración Django
    Keycloak -> Servers
    url             = https://sso.st.cl
    internal url    = 

```

``` 
    Keycloak -> Realms
    name            = AFC
    client_id       = django_client
    secret          = bfcfe601-b20e-4e16-908d-6bd5012413f1

```

Para mas información del lenguaje consultar la documentacuón y ejemplos en:
http://docs.python.org.ar/tutorial/3/index.html


## Ejecutar La apliación

```Bash
    python manage.py runserver
```

creación del usuario de administración:
```Bash
    python manage.py createsuperuser
```

Para mas información del lenguaje consultar la documentacuón y ejemplos en:
http://docs.python.org.ar/tutorial/3/index.html


# Equipo
- Ivonne Cardenas
- Rodrigo Carreño   (Owner)
- Francisco Castillo
- Franco Diaz
- Patricio Duran


# Estado del proyecto
El proyecto se encuentra en un estado inicial. Al cual se pueden agregar nuevas funcionalidades según sea requerido por el negocio.


# Licencia de Software Comercial
El uso, distribución o modificación están prohibidos o necesitan una autorización exclusiva de Overkode.com

[www.st.cl](http://www.st.cl)