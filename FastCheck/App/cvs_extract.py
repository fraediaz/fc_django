import csv
from AppDocumentos.models import DocumentoTemporal, Documento, Causal
from django.conf import settings
from django.db import IntegrityError
import os
media_path = settings.MEDIA_ROOT


def importar(doc):
    cc      = int(((doc.causal)[0:3]))
    ccc     = Causal.objects.get(articulo=cc, inciso=1)
    
    documento       = Documento.objects.create(
        pdf         = doc.pdf,
        rut         = doc.rut,
        rut_e       = doc.rut_e,
        nombre      = doc.nombre,
        nombre_e    = doc.nombre_e,
        fecha       = doc.fecha,
        importado   = True,
        escaneado   = True,
        causal      = ccc,
        num_solicitud = doc.solicitud
        
    )
    documento.save()

def bulk_data(cvs):
    with open(str(media_path)+cvs, newline='') as excel:
        lectura = csv.reader(excel, delimiter=',', quotechar='|')
        for x in lectura:
       
            fecha = ((x[5])[:4]+'-'+(x[5])[4]+(x[5])[5]+'-'+(x[5])[6:])
            sn    = False
            if (x[7]) == 'S':
                sn = True            
            try:
                documento   = DocumentoTemporal.objects.create(
                solicitud   = str((x[0])),
                nombre      = str((x[2])),
                rut         =str((x[1])),
                nombre_e    =str((x[4])),
                rut_e       =str((x[3])),
                causal      =str((x[6])),
                sn=sn,
                fecha=fecha,
                noseque=str((x[8])),
                pdf=str((x[9])),
                )   
                documento.save()
                importar(documento)
                
            except IntegrityError as identifier:
                print(identifier)
                pass
    
