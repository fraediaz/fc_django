from django.shortcuts import render
from django.views.generic.edit import UpdateView
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect

from AppClientes.models import Cliente
from .utilidades import ComprobarPerfil, Comprobar_estado_aws, Comprobar_resultados
from AppDocumentos.formularios import DocumentoForm, CSVForm
from AppDocumentos.models import Documento, DocumentoTemporal
from AppAWS import upload_s3
from App.cvs_extract import bulk_data

import random



@login_required(login_url='keycloak_login')
def NuevoDocumentoView(request):
    data            =       {
        'cliente'   :ComprobarPerfil(request.user),
        'form'      : DocumentoForm(),
        'documentos': Documento.objects.all(),
    }
    if request.method == "POST":
        form = DocumentoForm(request.POST, request.FILES)
        if form.is_valid():
            new_doc                 = form.save(commit=False)
            new_doc.usuario         = data['cliente']
            
            new_doc.save()
            upload_s3.subir_pdf(str(new_doc.pdf)[5:])
            
            jobid                   = upload_s3.analizar(str(new_doc.pdf)[5:])
            new_doc.id_aws          = jobid
            new_doc.estado          = upload_s3.estado_pdf(jobid)
            new_doc.save()
            return redirect('Index')
    return render(request, 'nuevo.html',data)


@login_required(login_url='keycloak_login')
def DocumentoView(request, _id):
    data            =       {
        'cliente'   :ComprobarPerfil(request.user),
        'documento' : Documento.objects.get(id=_id),
    }
    print(data['documento'])
    return render(request, 'documento.html',data)



@login_required(login_url='keycloak_login')
def DocumentoManual(request, _id):
    data            =       {
        'cliente'   :ComprobarPerfil(request.user),
        'form'      : DocumentoForm(),
        'documentos': Documento.objects.all(),
    }
    if request.method == "POST":
        form = DocumentoForm(request.POST, request.FILES)
        if form.is_valid():
            doc     = Documento.objects.filter(id=_id)
            doc.update(nombre=form.cleaned_data['nombre'])
            doc.update(tipo=form.cleaned_data['tipo'])
            doc.update(rut=form.cleaned_data['rut'])
            doc.update(rut_e=form.cleaned_data['rut_e'])
            doc.update(nombre_e=form.cleaned_data['nombre_e'])
            doc.update(fecha=form.cleaned_data['fecha'])
            doc.update(usuario=(Cliente.objects.get(user=request.user)))
            doc.update(escaneado=False)
            
            return redirect('Index')
    else:
        doc     = Documento.objects.get(id=_id)
        form    = DocumentoForm(instance=doc)
        data            =       {
        'cliente'   :ComprobarPerfil(request.user),
        'form'      : form,
        'documentos': Documento.objects.all(),
    }
    return render(request, 'editar.html',data)



@login_required(login_url='keycloak_login')
def InicioView(request):
    Comprobar_estado_aws()
    data        = {
        'cliente':ComprobarPerfil(request.user),
        'documentos': Documento.objects.all().order_by('-fecha_registro')
        }
    return render(request, 'index.html',data)


@login_required(login_url='keycloak_login')
def SubirCSV(request):
    data        = {
        'cliente'   :ComprobarPerfil(request.user),
        'form'      :CSVForm(),
        'documentos': DocumentoTemporal.objects.all()
        }
    if request.method == "POST":
        form = CSVForm(request.POST, request.FILES)
        if form.is_valid():
            new_doc                 = form.save(commit=False)
            new_doc.save()
            bulk_data(str(new_doc.csv))
            return redirect('Index')
            

    return render(request, 'subircsv.html',data)


@login_required(login_url='keycloak_login')
def DocumentosTemporalesView(request):
    Comprobar_estado_aws()
    data        = {
        'cliente':ComprobarPerfil(request.user),
        'documentos': DocumentoTemporal.objects.all()
        }
    return render(request, 'documentos_temporales.html',data)