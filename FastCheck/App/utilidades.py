from AppClientes.models import Cliente
from AppDocumentos.models import Documento
from AppAWS import upload_s3
from django.utils.formats import date_format


def ComprobarPerfil(usuario):
    try:
        cliente     = Cliente.objects.get(user=usuario)
    except:
        cliente     = Cliente.objects.create(user=usuario)
    cliente         = Cliente.objects.get(user=usuario)
    return cliente

def Comprobar_estado_aws():
    for x in Documento.objects.filter(escaneado=False):
        x.estado = upload_s3.estado_pdf(x.id_aws)
        x.save()
    Comprobar_resultados()

def Comprobar_resultados():
    for x in Documento.objects.filter(estado = 'SUCCEEDED', escaneado = False):
        x.pdf_texto=upload_s3.texto_pdf(x.id_aws)
        texto = x.pdf_texto.lower()
        
        try:
            texto.replace(" ", "")
            texto.replace("numero", "n")
            if texto.find(x.nombre.lower()) != (-1):
                x.nombre_match=True
                x.save()
            if texto.find(x.nombre_e.lower()) != (-1):
                x.nombre_e_match=True
                x.save()
            if texto.find(str(x.rut)) != (-1):
                x.rut_match=True
                x.save()
            if texto.find(str(x.rut_e)) != (-1):
                x.rut_e_match=True
                x.save()
            if texto.find(x.causal.nombre.lower()) != (-1):
                x.causal_match=True
            if texto.find('articulo'+str(x.causal.articulo)) != (-1):
                x.causal_match=True
            
            print((date_format(x.fecha).lower()).replace('de','hola', -1))
            
            x.save()
        except expression as identifier:
            pass   
        x.escaneado = True
        x.save()

