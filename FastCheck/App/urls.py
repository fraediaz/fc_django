from django.urls import path
from .views import *
urlpatterns = [
    path('', InicioView, name='Index'),
    path('nuevo',     NuevoDocumentoView, name='nuevo_finiquito'),
    path('documento/<str:_id>', DocumentoView,),
    path('editar/<str:_id>', DocumentoManual),
    path('subircsv', SubirCSV),
    path('temporales', DocumentosTemporalesView),
  
]
