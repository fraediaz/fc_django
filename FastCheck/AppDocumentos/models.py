from django.db import models
from AppClientes.models import Cliente
from django.urls import reverse
import uuid


OPC_DOCUMENTO = (
    (1, 'Finiquito'),
    (2, 'Carta de despido'),
    (3, 'Carta de renuncia'),
    (4, 'Certificado de defunción'),
    (5, 'Pensionado'),
    
)


class Causal(models.Model):
    nombre              = models.CharField('Nombre causal', max_length=64)
    articulo            = models.PositiveSmallIntegerField('Artículo')
    inciso              = models.PositiveSmallIntegerField('Inciso', blank=True, null=True)
    
    class Meta:
        verbose_name_plural = 'Causales'
        ordering = ['articulo', 'inciso']

    def __str__(self):
        return f'Art. {self.articulo}.{self.inciso} - {self.nombre}'
    
class CombinacionCausal(models.Model):
    causal          = models.ForeignKey(Causal, on_delete=models.CASCADE)
    texto           = models.CharField('Texto', max_length=60)
    def __str__(self):
        return f'{self.causal} - {self.texto}'

    class Meta:
        db_table = ''
        managed = True
        verbose_name = 'CombinacionCausal'
        verbose_name_plural = 'CombinacionCausales'


class Documento(models.Model):

    id                      = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    id_aws                  = models.CharField('Id AWS', max_length=128, blank=True, null=True)

    usuario                 = models.ForeignKey(Cliente, on_delete=models.SET_NULL,blank=True, null=True)

    pdf                     = models.FileField('PDF', upload_to='PDFs/', blank=True, null=True)
    pdf_texto               = models.TextField('Texto PDF', blank=True, null=True)

    tipo                    = models.PositiveSmallIntegerField('Tipo de documento', choices=OPC_DOCUMENTO, default=1, blank=True, null=True)

    rut                     = models.CharField('Rut', max_length=13)
    rut_match               = models.BooleanField('Rut match', default=False)

    nombre                  = models.CharField('Nombre', max_length=128)
    nombre_match            = models.BooleanField('Nombre match', default=False)

    nombre_e                = models.CharField('Nombre empresa', max_length=128)
    nombre_e_match          = models.BooleanField('Nombre empresa match', default=False)

    rut_e                   = models.CharField('Rut Empresa', max_length=13)
    rut_e_match             = models.BooleanField('Rut match', default=False)

    fecha                   = models.DateField('Fecha término de contrato')
    fecha_match             = models.BooleanField('Fecha match', default=False)

    causal                  = models.ForeignKey(Causal, max_length=64, on_delete=models.DO_NOTHING, default=2)
    causal_match            = models.BooleanField('Causal match', default=False)

    estado                  = models.CharField('Estado', max_length=20, blank=True, null=True)
    escaneado               = models.BooleanField('Escaneado por Textract', default=False)
    
    ok                      = models.BooleanField('Ok?', default=False)
    
    importado               = models.BooleanField('Pertenece a grupos de datos importados', default=False)
    
    num_solicitud           = models.PositiveIntegerField(blank=True, null=True)
    
    fecha_registro          = models.DateTimeField(auto_now=True)
    modificacion_registro   = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.nombre} {self.usuario}'
    def get_absolute_url(self):
        return reverse('documento-detail', kwargs={'pk': self.pk})
    
    
class DocumentoTemporal(models.Model):
    solicitud               = models.CharField('Solicitud', max_length=128, primary_key=True)
    nombre                  = models.CharField('Nombre', max_length=128)
    rut                     = models.CharField('Rut', max_length=12)
    nombre_e                = models.CharField('Nombre empresa', max_length=128)
    rut_e                   = models.CharField('Rut Empresa', max_length=12)
    causal                  = models.CharField('Causal', max_length=12)
    sn                      = models.BooleanField('S/N', default=False)
    fecha                   = models.DateField('Fecha')
    noseque                 = models.CharField('', max_length=128)
    pdf                     = models.CharField('PDF', max_length=128)

    def __str__(self):
        return f'{self.solicitud} {self.fecha}' 
    class Meta:
        verbose_name = 'Documento Temporal'
        verbose_name_plural = 'Documentos Temporales'
        
class CSVExtract(models.Model):
    csv                     = models.FileField('CSV', upload_to='csv/')
    def __str__(self):
        return 'ok'
