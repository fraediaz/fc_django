from django.contrib import admin
from .models import Documento, Causal, CombinacionCausal, DocumentoTemporal, CSVExtract
# Register your models here.
admin.site.register(Documento)
admin.site.register(Causal)
admin.site.register(CombinacionCausal)
admin.site.register(DocumentoTemporal)
admin.site.register(CSVExtract)