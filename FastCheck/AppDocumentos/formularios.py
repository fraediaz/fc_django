from django import forms
from AppDocumentos.models import Documento, CSVExtract


class DocumentoForm(forms.ModelForm):
    class Meta:
        model   = Documento   
        fields  = ('pdf','tipo', 'nombre', 'rut', 'nombre_e', 'rut_e','causal', 'fecha',)
        widgets = {
            'pdf'       : forms.ClearableFileInput(attrs={"class":"form-control"}),
            'tipo'      : forms.Select(attrs={"class":"form-control"}),
            'nombre'    : forms.TextInput(attrs={"class":"form-control", "placeholder":"Nombre completo"}),
            'rut'       : forms.NumberInput(attrs={"class":"form-control", "placeholder":"Rut"}),
            'nombre_e'  : forms.TextInput(attrs={"class":"form-control",  "placeholder":"Nombre empleador"}),
            'rut_e'     : forms.NumberInput(attrs={"class":"form-control",  "placeholder":"Rut empleador"}),
            'fecha'     : forms.DateInput(attrs={"class":"form-control", "id":"datepicker-1",  "placeholder":"Fecha desvinculación", "data-date-format":"dd/mm/yyyy"}),
            'causal'      : forms.Select(attrs={"class":"form-control"}),
        }

class CSVForm(forms.ModelForm):
    class Meta:
        model   = CSVExtract   
        fields  = ('csv',)
        widgets = {
            'csv'       : forms.ClearableFileInput(attrs={"class":"form-control"}),
           
        }
