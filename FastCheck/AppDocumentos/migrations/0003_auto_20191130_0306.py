# Generated by Django 2.2.7 on 2019-11-30 06:06

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('AppDocumentos', '0002_auto_20191130_0239'),
    ]

    operations = [
        migrations.AlterField(
            model_name='documento',
            name='fecha_match',
            field=models.BooleanField(default=False, verbose_name='Fecha match'),
        ),
        migrations.AlterField(
            model_name='documento',
            name='usuario',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL),
        ),
    ]
