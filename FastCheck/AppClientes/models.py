from django.db import models
from django.contrib.auth.models import User

class Cliente(models.Model):
    user        = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar      = models.ImageField('Avatar', upload_to='clientes/avatar/', default='clientes/avatar/default.jpg', blank=True, null=True)
    is_admin    = models.BooleanField('es administrador?', default=False)
    def __str__(self):
        return f'{self.user.first_name}'
