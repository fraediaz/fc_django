import boto3
from botocore.exceptions import ClientError

# import ipdb


class Comprehend():

    @staticmethod
    def detect_entities(text, lang):

        client = boto3.client('comprehend')
        response = None
        try:
            response = client.detect_entities(
                Text=text, LanguageCode=lang)

            # # location = {}
            # # organization = {}
            # # quantity={}
            # # date = {}

            # obj = {}
            # response = {}

            # for r in resp['Entities']:
            #     # if e['Type'] == 'LOCATION':
            #     # location = b['Text'] + '\n'

            #     obj.update({'Type': r['Type'], 'Text': r['Text']})
            #     response.update(obj)

            return response

        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print('No está autorizado a realizar esta operación')
                return False
            else:
                print('Error other than Not Found occurred: ' +
                      e.response['Error']['Message'])

        return response
