import boto3
#from AppDocumentos.models   import Documento
import time
import os


#
tiempo_inicio   = time.time()
#
#
textract    = boto3.client('textract')
s3          = boto3.resource('s3')
#
def subir_pdf(pdf):
    s3.Object('afcdjango', pdf).upload_file('assets/media/PDFs/'+pdf)

def analizar(pdf):
    response    = None
    response    = textract.start_document_text_detection(
    DocumentLocation={
        'S3Object': {
            'Bucket': 'afcdjango',
            'Name': pdf
        }
    })
    return response["JobId"]

def texto_pdf(id_aws):
    response    = textract.get_document_text_detection(JobId=id_aws)
    texto       = ""
    if response['JobStatus'] == 'SUCCEEDED':
        bloques     = response['Blocks']
        for x in bloques:
            if x['BlockType'] == 'LINE':
                texto += x['Text'] 
        return  texto

def estado_pdf(id_aws):
    response    = textract.get_document_text_detection(JobId=id_aws)
    status      = response["JobStatus"]
    return status