import boto3
from botocore.exceptions import ClientError
# from os import environ

# import ipdb; ipdb.set_trace()


class Collections():
    '''  AWS API Collections  '''

    @staticmethod
    def Exists(collectionId):
        ''' Verifica si Existe Colecccion en AWS '''

        print('[INFO] Accediendo a la colección ' + collectionId)
        client = boto3.client('rekognition')
        try:
            # response = False
            client.describe_collection(CollectionId=collectionId)

            # import ipdb
            # ipdb.set_trace()

            return True

        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print('No está autorizado a realizar esta operación')
                return False
            else:
                print('Error other than Not Found occurred: ' +
                      e.response['Error']['Message'])

    @staticmethod
    def Detail(collectionId):
        ''' AWS Obtiene información de una colección '''
        print('Detail: Accediendo a la colección ' + collectionId)
        client = boto3.client('rekognition')

        try:
            response = client.describe_collection(CollectionId=collectionId)
            print("Collection Arn: " + response['CollectionARN'])
            print("Face Count: " + str(response['FaceCount']))
            print("Face Model Version: " + response['FaceModelVersion'])
            print("Timestamp: " + str(response['CreationTimestamp']))

            return response

        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                print('The collection ' + collectionId + ' was not found ')
            else:
                print('Error other than Not Found occurred: ' +
                      e.response['Error']['Message'])

    @staticmethod
    def Create(collectionId):
        ''' AWS Crear una nueva colección '''
        # maxResults = 2
        client = boto3.client('rekognition')

        try:
            # Create a collection
            print('[INFO ]Creando colección: ' + collectionId)
            response = client.create_collection(CollectionId=collectionId)
            # print('Collection ARN: ' + response['CollectionArn'])
            # print('Status code: ' + str(response['StatusCode']))
            print('[INFO] Creación de la colección',
                  collectionId, ' Finalizado...')

            return response
        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                print('[ERROR] No se ha encontrado la colección ',
                      collectionId)
                return False
            else:
                print('[ERROR] Se ha producido una Excepción: ' +
                      e.response['Error']['Message'])

    @staticmethod
    def Delete(coleccion):
        ''' Eliminar la colección de amazon web service '''
        client = boto3.client('rekognition')

        try:
            client.delete_collection(CollectionId=coleccion)

            # import ipdb; ipdb.set_trace()

            print('[INFO] Delete ', coleccion, ' Finalizado...')
            return True

        except ClientError as e:
            if e.response['Error']['Code'] == 'ResourceNotFoundException':
                print('[ERROR] No se ha encontrado la colección ',  coleccion)
                return False
            else:
                print('[ERROR] Se ha producido una Excepción: ' +
                      e.response['Error']['Message'])

    @staticmethod
    def Index_Faces(collectionId, uploaded_file_url, external):
        client = boto3.client('rekognition')

        response = client.index_faces

        print('AWS Index_facses Starting...............')
        print('collectionId: ', collectionId)
        print('external: ', external)
        print('uploaded_file_url: ', uploaded_file_url)
        # import ipdb; ipdb.set_trace()

        with open(uploaded_file_url, 'rb') as source_image:
            source_bytes = source_image.read()

        response = client.index_faces(CollectionId=collectionId,
                                      Image={'Bytes': source_bytes},
                                      ExternalImageId=external,
                                      MaxFaces=1,
                                      QualityFilter="AUTO",
                                      DetectionAttributes=['ALL'])

        # print('******************** AWS RESPONSE **************************')
        # print(response)
        # print('************************************************************')
        # print('Results for ', external)
        # print('Faces indexed:')
        # for faceRecord in response['FaceRecords']:
        #     print('  Face ID: ' + faceRecord['Face']['FaceId'])
        #     print('  Location: {}'.format(faceRecord['Face']['BoundingBox']))

        # print('Faces not indexed:')
        # for unindexedFace in response['UnindexedFaces']:
        #     print(' Location: {}'.
        #           format(unindexedFace['FaceDetail']['BoundingBox']))
        #     print(' Reasons:')
        #     for reason in unindexedFace['Reasons']:
        #         print('   ' + reason)
        return response
