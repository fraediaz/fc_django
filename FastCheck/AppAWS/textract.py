import boto3
from botocore.exceptions import ClientError

# import ipdb


class Textract():
    @staticmethod
    def start_document_text_detection(
            snsTopicArn, RolArn, bucket, document):

        client = boto3.client('textract')
        response = None
        try:
            response = client.start_document_text_detection(
                DocumentLocation={
                    'S3Object': {
                        'Bucket': bucket,
                        'Name': document
                        # 'Version': 'string'
                    }
                },
                # ClientRequestToken='string',
                JobTag='string',
                NotificationChannel={
                    'SNSTopicArn': snsTopicArn,
                    'RoleArn': RolArn
                }
            )
        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print('No está autorizado a realizar esta operación')
                return False
            else:
                print('Error other than Not Found occurred: ' +
                      e.response['Error']['Message'])

        return response

    @staticmethod
    def get_document_text_detection(job, maxResult=1000):

        client = boto3.client('textract')
        response = {}
        try:
            respo = client.get_document_text_detection(
                JobId=job,
                MaxResults=maxResult)

            # Obtener Líneas de texto
            texto = ""
            if respo['JobStatus'] == "SUCCEEDED":
                bloques = respo['Blocks']
                for b in bloques:
                    if b['BlockType'] == 'LINE':
                        texto += b['Text'] + '\n'
                        # texto += b['Text']

            # Preparar La respuesta limpia
            response['JobStatus'] = respo['JobStatus']
            response['CleanText'] = texto

        except ClientError as e:
            if e.response['Error']['Code'] == 'AccessDeniedException':
                print('No está autorizado a realizar esta operación')
                return False
            else:
                print('Error other than Not Found occurred: ' +
                      e.response['Error']['Message'])

        return response
